def gender(x)
  
  if x == "male"
    return "Hello, Sir!"
  elsif x == "female"
    return "Hello, Ma'am!"
  else
    return "Hello there!"
  end
end

puts "Enter your gender"
answer = gets.chomp

puts "#{gender(answer)}"