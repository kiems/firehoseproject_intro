
def ordinal(answer)
  our_ordinal = answer % 10

    if our_ordinal == 1
      return "#{answer}st"
    elsif our_ordinal == 2
      return "#{answer}nd"
    elsif our_ordinal == 3
      return "#{answer}rd"
    else
      return "#{answer}th"
    end
end



puts "Enter a number"
answer = gets.chomp.to_i

puts "Hi!  The number was #{ordinal(answer)}"