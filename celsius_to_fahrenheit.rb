def convert_to_fahrenheit(a)
  
  #T(°F) = T(°C) × 9/5 + 32
  fahrenheit = (a * 9/5) + 32
  return fahrenheit
end

puts "Enter degress in celsius?"
celsius = gets.chomp.to_f

puts convert_to_fahrenheit(celsius)
