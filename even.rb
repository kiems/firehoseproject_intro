def is_even?(n)
  remainder_when_divided_by_2 = n % 2
  
  if remainder_when_divided_by_2 == 0
    return true
  else
    return false
  
  end
end

def is_odd?(n)
  return ! is_even?(n)
  
end

def is_even_and_divisible_by_five?(n)
  
  remainder_when_divisible_by_5 = n % 5
  
  if is_even?(n) && remainder_when_divisible_by_5 == 0
    return true
  else
    return false
  end
  
end

puts "1 is_even? #{is_even?(15)} - is_odd? #{is_odd?(15)} - is_even_and_divisible_by_five? #{is_even_and_divisible_by_five?(15)}"
puts "2 is_even? #{is_even?(25)} - is_odd? #{is_odd?(25)} - is_even_and_divisible_by_five? #{is_even_and_divisible_by_five?(25)}"
puts "3 is_even? #{is_even?(35)} - is_odd? #{is_odd?(35)} - is_even_and_divisible_by_five? #{is_even_and_divisible_by_five?(35)}"
puts "4 is_even? #{is_even?(45)} - is_odd? #{is_odd?(45)} - is_even_and_divisible_by_five? #{is_even_and_divisible_by_five?(45)}"
puts "5 is_even? #{is_even?(55)} - is_odd? #{is_odd?(55)} - is_even_and_divisible_by_five? #{is_even_and_divisible_by_five?(55)}"
puts "6 is_even? #{is_even?(68)} - is_odd? #{is_odd?(68)} - is_even_and_divisible_by_five? #{is_even_and_divisible_by_five?(68)}"
